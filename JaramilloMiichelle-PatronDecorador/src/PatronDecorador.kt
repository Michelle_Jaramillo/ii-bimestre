package patterns

interface ServicioCarro {
    fun doService()
}

interface CarServiceDecorator : ServicioCarro

class ServicioBasico : ServicioCarro {
    override fun doService() = println("Haciendo chequeo básico ... HECHO ")
}

class LavarCarro(private val carService: ServicioCarro) : CarServiceDecorator {
    override fun doService() {
        carService.doService()
        println("Lavando el carro ... HECHO")
    }
}

class PulirCarro(private val carService: ServicioCarro) : CarServiceDecorator {
    override fun doService() {
        carService.doService()
        println("Pulir carro ... HECHO")
    }
}

class LimpiezaInterior(private val carService: ServicioCarro) : CarServiceDecorator {
    override fun doService() {
        carService.doService()
        println("Limpiando el carro por dentro ... HECHO")
    }
}

fun main(args: Array<String>) {
    val servicioCarro = LimpiezaInterior(LavarCarro(PulirCarro(ServicioBasico())))
    servicioCarro.doService()
}