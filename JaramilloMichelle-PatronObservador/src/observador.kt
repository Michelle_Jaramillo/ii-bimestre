package observador

import java.util.*

class HAMBURGUESA(val nombreBurger: String)

class Chef : Observable() {

    val nombreObservable= "Bob"

    fun cookBurger(nombreHamburguesa : String){
        var hamburguesa = HAMBURGUESA(nombreHamburguesa)

        setChanged()
        notifyObservers(hamburguesa)
    }
}

class Mesero : Observer{

    val nombreObserver = "Tina"

    override fun update(o: Observable?, arg: Any?) {
        when (o){
            is Chef -> {
                if (arg is HAMBURGUESA){
                    println("$nombreObserver está sirviendo la ${arg.nombreBurger} cocinada por ${o.nombreObservable}")
                }
            }
            else -> println(o?.javaClass.toString())
        }
    }
}

fun main(args : Array<String>){
    val bob = Chef()
    bob.addObserver(Mesero())

    bob.cookBurger("'Cheese Burger'" )
}