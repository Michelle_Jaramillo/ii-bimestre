object Usuario {
    var name = "Michelle"

    init {
        println("Singleton invoked")
    }

    fun printName() = println(name)
}